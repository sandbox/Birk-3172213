<?php

namespace Drupal\activegroup;

use Drupal\Component\Plugin\PluginBase;

/**
 * Active method plugin base.
 */
abstract class ActivegroupMethodBase extends PluginBase implements ActivegroupMethodInterface {

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupIds() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isReady() {
    return TRUE;
  }

}
