<?php

namespace Drupal\activegroup;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Activegroup method interface.
 */
interface ActivegroupMethodInterface extends PluginInspectionInterface {

  /**
   * Get group ids.
   *
   * @return int[]
   *   List of group ids.
   */
  public function getGroupIds();

  /**
   * A form with the settings for the method.
   *
   * @return array
   *   A form array.
   */
  public function settingsForm();

  /**
   * Is the method valid for the current request.
   *
   * @return bool
   *   TRUE if the method is runnable.
   */
  public function isValid();

  /**
   * If the method is ready yet or not.
   *
   * @return bool
   *   TRUE if the method is ready.
   */
  public function isReady();

}
