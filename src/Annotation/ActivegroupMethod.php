<?php

namespace Drupal\activegroup\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an ActivegroupMethod annotation object.
 *
 * @Annotation
 */
class ActivegroupMethod extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * Description.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description = '';

}
