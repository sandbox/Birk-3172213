<?php

namespace Drupal\activegroup;

use Drupal\Component\Utility\SortArray;
use Drupal\group\Entity\Group;

/**
 * Manages activegroup methods.
 */
class Activegroup {

  /**
   * Static cache for the active groups.
   *
   * @var \Drupal\group\Entity\GroupInterface[]
   */
  protected $groups;

  /**
   * Static cache for active group ids.
   *
   * @var int[]
   */
  protected $groupIds;

  /**
   * Static cache for the methods.
   *
   * @var ActivegroupMethodInterface[]
   */
  protected $methods;

  /**
   * Activegroup method manager instance.
   *
   * @var ActivegroupMethodManager
   */
  protected $methodManager;

  /**
   * Avoid ready checking loops.
   *
   * @var bool
   * @see isReady()
   */
  protected $isReadySemaphore = FALSE;

  /**
   * Is it ready.
   *
   * @var bool
   */
  protected $ready = FALSE;

  protected $override = FALSE;

  /**
   * Create instance.
   */
  public function __construct(ActivegroupMethodManager $method_manager = NULL) {
    $this->methodManager = $method_manager;
  }

  /**
   * Get groups.
   *
   * @return \Drupal\group\Entity\GroupInterface[]
   *   List of groups.
   */
  public function getGroups() {
    if ($this->override !== FALSE) {
      return [Group::load($this->override)];
    }

    if (!isset($this->groups)) {
      // The entityTypeManager is not injected, this is done to avoid dependency
      // loops.
      $groups = \Drupal::entityTypeManager()->getStorage('group')->loadMultiple($this->getGroupIds());

      if ($this->isReady()) {
        return $groups;
      }

      $this->groups = $groups;
    }

    return $this->groups;
  }

  /**
   * Get the first active group.
   *
   * @return \Drupal\group\Entity\GroupInterface|false
   *   The group.
   */
  public function getGroup() {
    $groups = $this->getGroups();
    return reset($groups);
  }

  /**
   *
   */
  public function setOverride($id) {
    $this->override = $id;

    return $this;
  }

  /**
   *
   */
  public function unsetOverride() {
    $this->override = FALSE;

    return $this;
  }

  /**
   * Get the first active group ID.
   *
   * @return int|false
   *   The ID.
   */
  public function getGroupId() {
    $ids = $this->getGroupIds();
    return reset($ids);
  }

  /**
   * Get the config.
   */
  public function getConfigs() {
    $storage = \Drupal::service('config.storage');

    $configs = [];
    foreach ($storage->listAll('activegroup.method.') as $name) {
      $config = $storage->read($name);

      if ($config['status'] === FALSE) {
        continue;
      }

      $configs[$config['id']] = $config;
    }

    // Sort by weight.
    uasort($configs, [SortArray::class, 'sortByWeightElement']);

    return $configs;
  }

  /**
   * Get methods.
   */
  public function getMethods() {
    if (!isset($this->methods)) {
      $this->methods = [];

      // Instantiate the method plugins.
      foreach ($this->getConfigs() as $config) {
        $method = $this->methodManager->createInstance($config['method'], $config['settings']);
        $this->methods[$config['id']] = $method;
      }
    }

    return $this->methods;
  }

  /**
   * Get group Ids.
   *
   * @return int[]
   *   List of group IDs.
   */
  public function getGroupIds() {
    if ($this->override !== FALSE) {
      return [$this->override];
    }


    if (!isset($this->groupIds)) {
      $this->groupIds = [];
      $ready = TRUE;
      $ids = [];

      foreach ($this->getMethods() as $method) {
        if ($method->isValid() === FALSE) {
          continue;
        }

        $ready = $ready && $method->isReady();

        $ids = $method->getGroupIds();
        if (!empty($ids)) {
          break;
        }
      }

      if ($ready === FALSE) {
        unset($this->groupIds);
        return $ids;
      }

      $this->groupIds = $ids;
      $this->ready = TRUE;
    }

    return $this->groupIds;
  }

  /**
   * Check if activegroups is ready to get the active group.
   *
   * @return bool
   *   TRUE if it's ready.
   */
  public function isReady() {
    if ($this->ready === FALSE && $this->isReadySemaphore === FALSE) {
      $this->isReadySemaphore = TRUE;

      $ready = TRUE;
      foreach ($this->getMethods() as $method) {
        $ready = $method->isReady();
        if ($ready === FALSE) {
          break;
        }
      }

      $this->ready = $ready;
      $this->isReadySemaphore = FALSE;
    }

    return $this->ready;
  }

}
