<?php

namespace Drupal\activegroup\Plugin\ActivegroupMethod;

use Drupal\activegroup\ActivegroupMethodBase;

/**
 * Fallback to a specific group.
 *
 * @ActivegroupMethod(
 *   id = "fallback",
 *   title = @Translation("Fallback"),
 *   description = @Translation("Fallback to a specic group.")
 * )
 */
class FallbackMethod extends ActivegroupMethodBase {

  /**
   * {@inheritdoc}
   */
  public function getGroupIds() {
    return [(int) $this->configuration['group']];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $group = NULL;
    if (!empty($this->configuration['group'])) {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($this->configuration['group']);
    }

    $form['group'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Group',
      '#target_type' => 'group',
      '#selection_settings' => [
        'match_operator' => 'STARTS_WITH',
      ],
      '#size' => '18',
      '#default_value' => $group,
      '#required' => TRUE,
    ];

    return $form;
  }

}
