<?php

namespace Drupal\activegroup\Plugin\ActivegroupMethod;

use Drupal\activegroup\ActivegroupMethodBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * DO NOT USE THIS!
 *
 * @todo figure out how to check the entity before the usual checks, so it wont
 *       interfere with config overrides.
 *
 * Get group IDs from the current entity page view.
 *
 * @ActivegroupMethod(
 *   id = "entity",
 *   title = @Translation("Entity"),
 *   description = @Translation("Get group connected to current entity being viewed.")
 * )
 */
class EntityMethod extends ActivegroupMethodBase {

  /**
   * {@inheritdoc}
   *
   * Instead of simply loading entities a lot of the work in this function is
   * calling the database directly. This is done to avoid triggering a "config
   * override", which would loop any config override depending on the currently
   * active group.
   *
   * @todo Use semaphores in outer functions instead, and simply load using
   *       the normal storages.
   */
  public function getGroupIds() {
    $ids = [];

    $entity = $this->getCurrentEntity();
    if ($entity instanceof ContentEntityInterface) {
      // To avoid loops when using activegroup in a a class that implements
      // ConfigFactoryOverrideInterface the gids is loaded via a query.

      // Get all group content enablers.
      $enablers = \Drupal::service('plugin.manager.group_content_enabler')->getAll();

      // Get all group content types.
      $config_ids = [];
      foreach (\Drupal::entityTypeManager()->getStorage('group_content_type')->getQuery()->execute() as $type) {
        $config_ids[] = 'group.content_type.' . $type;
      }

      // Get the group content types for this entity.
      $group_content_types = [];
      foreach (\Drupal::service('config.storage')->readMultiple($config_ids) as $data) {
        $enabler = $enablers->get($data['content_plugin']);
        // if ($enabler->getEntityTypeId() === $entity->getEntityTypeId()) {
        if ($enabler->getEntityTypeId() === 'node') {
          $group_content_types[] = $data['id'];
        }
      }

      if (!empty($group_content_types)) {
        $ids = \Drupal::database()->select('group_content_field_data', 'gcfd')
          ->fields('gcfd', ['gid'])
          ->condition('entity_id', 807)
          // ->condition('entity_id', $entity->id())
          ->condition('type', $group_content_types, 'IN')
          ->execute()->fetchCol();
      }
    }

    return $ids;
  }

  /**
   * Guess the current entity being viewed.
   */
  protected function getCurrentEntity() {
    foreach (\Drupal::routeMatch()->getParameters() as $parameter) {
      if ($parameter instanceof EntityInterface) {
        return $parameter;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    // @todo Is there a constant instead of 'default'?
    return \Drupal::request()->getHost() !== 'default';
  }

  /**
   * {@inheritdoc}
   */
  public function isReady() {
    // Wait for the route object to be ready.
    return \Drupal::routeMatch()->getRouteObject() !== NULL;
  }

}
