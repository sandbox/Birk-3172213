<?php

namespace Drupal\activegroup\Plugin\views\argument_default;

use Drupal\activegroup\Activegroup;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default argument plugin to extract the active group ID.
 *
 * @ViewsArgumentDefault(
 *   id = "activegroup_id",
 *   title = @Translation("Active group ID")
 * )
 */
class ActivegroupId extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  protected $activegroup;

  /**
   * Constructs a new GroupIdFromUrl instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Activegroup $activegroup) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->activegroup = $activegroup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('activegroup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    return $this->activegroup->getGroupId();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @todo Get from methods.
    return ['url.site'];
  }

}
