<?php

namespace Drupal\activegroup\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * A simple block that shows the currently active group.
 *
 * @Block(
 *   id = "activegroup_block",
 *   admin_label = @Translation("Activegroup detector"),
 * )
 */
class ActivegroupBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $results = [];

    /** @var \Drupal\group\Entity\Storage\GroupStorage */
    $group_storage = \Drupal::entityTypeManager()->getStorage('group');
    /** @var \Drupal\activegroup\Activegroup */
    $activegroup = \Drupal::service('activegroup');

    $configs = $activegroup->getConfigs();
    foreach ($activegroup->getMethods() as $config_id => $method) {
      $groups = [];

      $gids = $method->getGroupIds();
      if (!empty($gids)) {
        foreach ($group_storage->loadMultiple($gids) as $group) {
          $groups[] = $group->label();
        }
      }

      $results[$config_id] = [
        '#markup' => '<strong>' . $configs[$config_id]['label'] . '</strong> [' . implode(', ', $groups) . ']',
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      ];
    }

    return $results;
  }

}
