<?php

namespace Drupal\activegroup\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Represents a text format.
 *
 * @ConfigEntityType(
 *   id = "activegroup_method",
 *   label = @Translation("Activegroup method"),
 *   label_collection = @Translation("Activegroup methods"),
 *   label_singular = @Translation("Activegroup method"),
 *   label_plural = @Translation("Activegroup methods"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Activegroup method",
 *     plural = "@count Activegroup methods",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\activegroup\Form\ActivegroupMethodAddForm",
 *       "edit" = "Drupal\activegroup\Form\ActivegroupMethodEditForm",
 *       "delete" = "Drupal\activegroup\Form\ActivegroupMethodDeleteForm"
 *     },
 *     "list_builder" = "Drupal\activegroup\ActivegroupMethodListBuilder"
 *   },
 *   config_prefix = "method",
 *   admin_permission = "administer activegroup methods",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *     "settings" = "settings",
 *     "status" = "status"
 *   },
 *   links = {
 *     "edit-form" = "/admin/group/activegroup/manage/{activegroup_method}",
 *     "delete-form" = "/admin/group/activegroup/manage/{activegroup_method}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "method",
 *     "weight",
 *     "settings",
 *   }
 * )
 */
class ActivegroupMethod extends ConfigEntityBase {

  /**
   * Identifier.
   *
   * @var string
   */
  protected $id;

  /**
   * Label.
   *
   * @var string
   */
  protected $label;

  /**
   * The method plugin to use.
   *
   * @var string
   */
  protected $method;

  /**
   * Method settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Method weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * Get the Activegroup method.
   *
   * @return \Drupal\activegroup\ActivegroupMethodInterface
   *   Instance.
   */
  public function getMethod() {
    return \Drupal::service('plugin.manager.activegroup')->createInstance($this->method, $this->settings);
  }

}
