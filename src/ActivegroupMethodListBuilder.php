<?php

namespace Drupal\activegroup;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder.
 */
class ActivegroupMethodListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $entitiesKey = 'id';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activegroup_methods_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];

    $header['label'] = $this->t('Label');
    $header['method'] = $this->t('Method');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];

    $row['label'] = $entity->label();
    $row['method']['#markup'] = $entity->get('method');

    return $row + parent::buildRow($entity);
  }

}
