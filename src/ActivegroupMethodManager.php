<?php

namespace Drupal\activegroup;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages activegroup methods.
 */
class ActivegroupMethodManager extends DefaultPluginManager {

  /**
   * Constructs a ActivegroupMethodManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ActivegroupMethod',
      $namespaces,
      $module_handler,
      'Drupal\activegroup\ActivegroupMethodInterface',
      'Drupal\activegroup\Annotation\ActivegroupMethod'
    );
    $this->alterInfo('activegroup_method_info');
    $this->setCacheBackend($cache_backend, 'activegroup_method');
  }

}
