<?php

namespace Drupal\activegroup\Form;

use Drupal\activegroup\ActivegroupMethodManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base form for a filter format.
 */
class ActivegroupMethodEditForm extends EntityForm {

  /**
   * Activegroup method manager.
   *
   * @var \Drupal\activegroup\ActivegroupMethodManager
   */
  protected $manager;

  /**
   * Class constructor.
   */
  public function __construct(ActivegroupMethodManager $manager) {
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.activegroup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#weight' => -30,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#required' => TRUE,
      '#default_value' => $this->entity->id(),
      '#maxlength' => 255,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['label'],
      ],
      '#disabled' => TRUE,
      '#weight' => -20,
    ];

    /** @var \Drupal\activegroup\ActivegroupMethodInterface */
    $method = $this->entity->getMethod();
    $settings_form = $method->settingsForm();
    if (!empty($settings_form)) {
      $form['settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@method settings', ['@method' => $method->getPluginDefinition()['title']]),
        '#tree' => TRUE,
      ] + $settings_form;
    }

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('activegroup.admin_overview');
  }

}
