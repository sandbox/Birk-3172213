<?php

namespace Drupal\activegroup\Form;

use Drupal\activegroup\ActivegroupMethodManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base form for a filter format.
 */
class ActivegroupMethodAddForm extends EntityForm {

  /**
   * Activegroup method manager.
   *
   * @var \Drupal\activegroup\ActivegroupMethodManager
   */
  protected $methodManager;

  /**
   * Class constructor.
   */
  public function __construct(ActivegroupMethodManager $method_manager) {
    $this->methodManager = $method_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.activegroup')
    );
  }

  /**
   * Check if the ID already exists.
   *
   * Used by the machine_name element.
   *
   * @return bool
   *   TRUE if the ID exists.
   */
  public function exists($id) {
    return (bool) $this->entityTypeManager
      ->getStorage('activegroup_method')->getQuery()
      ->condition('id', $id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#required' => TRUE,
      '#weight' => -30,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#required' => TRUE,
      '#maxlength' => 255,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['label'],
      ],
      '#weight' => -20,
    ];

    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('method'),
      '#required' => TRUE,
      '#options' => [],
    ];
    foreach ($this->methodManager->getDefinitions() as $plugin_id => $definition) {
      $form['method']['#options'][$plugin_id] = $definition['title'];
    }

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
