CONTENTS OF THIS FILE
=====================

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Determine the currently active group(s), using a weighted plugin system.


REQUIREMENTS
------------

This module requires the following modules:

 * Group (https://www.drupal.org/project/group)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

NOT FILLED YET.


MAINTAINERS
-----------

* Birk (Birk) - https://www.drupal.org/u/birk
